from django.forms import ModelForm
from myapp.models import Article

CATEGORY = [('Quiz','Quiz'),
            ('Exam','Exam'),
            ('Homework','Homework'),
            ('Meeting','Meeting'),
            ('Others','Others')]

class Schedule(models.Model):
    dates = models.DataField()
    time = models.TimeField()
    activity = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50, choices = CATEGORY)
    model = Article
    fields = ['pub_date', 'headline', 'content', 'reporter']

# Creating a form to add an article.
form = ArticleForm()

# Creating a form to change an existing article.
article = Article.objects.get(pk=1)
form = ArticleForm(instance=article),
