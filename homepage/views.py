from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime


def index(request):
    return render(request, 'index.html')
def base(request):
    return render(request, 'base.html')
def profile(request):
    return render(request, 'profile.html')
def education(request):
    return render(request, 'education.html')
def experience(request):
    return render(request, 'experience.html')
def contacts(request):
    return render(request, 'contacts.html')
def escape(request):
    return render(request, 'escape.html')
def Schedule(request):
    if request.method =='POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('sched_detail')
        else:
            form = ScheduleForm()

    return render(request, 'Schedule.html')
