from django import forms
from .models import Schedule
from django.forms import ModelForm

class ScheduleForm(ModelForm):
    class Meta:
        model= Schedule
        fields = ['dates', 'time', 'activity', 'location', 'category']
        labels = {
            'dates' : 'Date', 'time' : 'Time', 'activity' : 'Activity', 'location' : 'Location', 'category' : 'Category'
        }
        widgets = {
            'dates' : forms.DateInput(attrs={'class': 'form-control',
                                        'type' : 'date'}),
            'time' : forms.TimeInput(attrs={'class': 'form-control',
                                        'type' : 'time'}),
            'activity' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Wat u gonna do'}),
            'location' : forms.TextInput(attrs={'class' : 'form-control',
                                        'type' : 'text',
                                        'placeholder' : 'Wer izit'}),
            'category' : forms.Select(attrs={'class': 'form-control'})
        }
