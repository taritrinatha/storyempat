from django.urls import path
from django.shortcuts import render
from . import views

storyempat = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('base/', views.base, name='base'),
    path('education/', views.education, name='education'),
    path('experience/', views.experience, name='experience'),
    path('contacts/', views.contacts, name='contacts'),
    path('escape/', views.escape, name='escape'),
    path('sched_add/', sched_add, name='sched_add'),
    path('sched_detail/', sched_detail, name='sched_detail'),
    path('sched_edit/<int:pk>', sched_edit, name='sched_edit'),
    path('sched_delete/<int:pk>', sched_delete, name='sched_delete'),
    path('del_sched/', del_sched, name='del_sched')

]
