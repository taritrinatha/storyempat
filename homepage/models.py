from django.db import models
import datetime

CATEGORY=[('Quiz', 'Quiz'),
            ('Exam', 'Exam'),
            ('Homework', 'Homework'),
            ('Hangout', 'Hangout'),
            ('Meeting', 'Meeting'),
            ('Others', 'Others')]

class Schedule(models.Model):
    dates = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50, choices=CATEGORY)
